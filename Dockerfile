FROM ubuntu

RUN apt update && apt -y install openjdk-21-jdk
ADD target/lib/dsm.deb ./dsm.deb
#RUN apt -y install telnet
#RUN apt -y install netcat
#RUN apt -y install tcpdump
#RUN apt -y install net-tools
RUN dpkg -i dsm.deb
