package us.techmentor.dsmintegration;

import java.io.File;
import java.nio.file.Paths;

public class LibraryMaintenance {
    public static void prepareDebianInstallation(){
        var currentRelativePath = Paths.get("target","lib").toFile();
        var file = currentRelativePath.listFiles((f,s) -> s.matches("dsm.*\\.deb"))[0];
        file.renameTo(new File("target/lib/dsm.deb"));
    }
}
