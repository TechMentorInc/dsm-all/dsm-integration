package us.techmentor.dsmintegration;

import static us.techmentor.dsmintegration.LibraryMaintenance.prepareDebianInstallation;

public class AWSDSMContainer extends DSMContainer {
    public AWSDSMContainer(String name,String shared) {
        super(name,shared,"");
    }
    public String start(){
        var cmd = "docker "+getHostCommand()+" run --net bridge -dit --name "+name+" test-image /bin/bash -c 'dsm init;/bin/bash'";
//        docker.runDockerCommandReadString(cmd);
                docker.runDockerCommand(cmd);
        return getIpAddress();
    }
    public void getDirectoryContents(){
        var cmd = "docker "+getHostCommand()+" exec -t "+name+" /bin/bash -c 'ls -a'";
        docker.runDockerCommand(cmd);
    }
}
