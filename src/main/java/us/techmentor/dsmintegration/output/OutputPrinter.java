package us.techmentor.dsmintegration.output;

import us.techmentor.dsmintegration.Docker;

public class OutputPrinter {
    private static final String ANSI_GREEN = "\u001B[32m";
    final String ANSI_RESET = "\u001B[0m";
    final String ANSI_RED = "\u001B[31m";

    void addToBuffer(OutputThreadBuffer buffer, String newString){
        buffer.buffer.append(newString);
        var trim = buffer.buffer.length()-100000;
        if(trim>0)
            buffer.buffer = new StringBuffer(buffer.buffer.substring(trim));
    }
    public void printStandard(Outputs o, OutputThreadBuffer buffer){
        try {
            String standardTag = ANSI_GREEN + "[" + o.name() + "/"+o.displayName()+"," + o.processCommandLine() + "] " + ANSI_RESET;
            if (o.standard().available() > 0) {

                var available = o.standard().available();
                var standardBytes = new byte[available];
                o.standard().read(standardBytes);
                var standardLines = new String(standardBytes).split("\n");

                for (int j = 0; j < standardLines.length; j++) {
                    var line = standardTag + standardLines[j];
                    System.out.println(line);
                    addToBuffer(buffer, line);
                }
            }
        }catch(Throwable t){
            throw new RuntimeException(t);
        }
    }
    public void printError(Outputs o, OutputThreadBuffer buffer){
        try {
            var errorTag = ANSI_RED + "[" + o.name() + "/"+o.displayName() +"," + o.processCommandLine() + "] " + ANSI_RESET;
            if (o.error().available() > 0) {
                var available = o.error().available();
                var errorBytes = new byte[available];
                o.error().read(errorBytes);
                var errorLines = new String(errorBytes).split("\n");
                for (int j = 0; j < errorLines.length; j++) {
                    var line = errorTag + errorLines[j];
                    System.out.println(line);
                    addToBuffer(buffer, line);
                }
            }
        }catch(Throwable t){
            throw new RuntimeException(t);
        }
    }
}
