package us.techmentor.dsmintegration.output;

public class OutputThreadBuffer {
    private final OutputThread outputThread;
    public StringBuffer buffer = new StringBuffer();
    public String asString(){return this.buffer.toString();}
    public void stop(){
        outputThread.stop();
    }
    public OutputThreadBuffer(OutputThread outputThread){
        this.outputThread = outputThread;
    }
    public OutputThread getOutputThread(){
        return this.outputThread;
    }
}
