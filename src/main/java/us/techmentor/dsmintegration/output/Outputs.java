package us.techmentor.dsmintegration.output;

import java.io.InputStream;

public record Outputs(String name, String displayName, String processCommandLine, InputStream standard, InputStream error){}