package us.techmentor.dsmintegration.output;

import us.techmentor.dsmintegration.DSMContainer;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class OutputThread {
    Thread t = null;
    AtomicBoolean done = new AtomicBoolean(false);
    OutputThreadBuffer outputThreadBuffer = new OutputThreadBuffer(this);
    OutputPrinter outputPrinter = new OutputPrinter();

    public OutputThreadBuffer getBuffer(){ return outputThreadBuffer; }

    public void start() { if(t!=null && !t.isAlive()) t.start(); }
    public void stop()  { done.set(true); }
    public void create(boolean printError, DSMContainer container){
        var t = new Thread(()->{
            while (!done.get()) {
                synchronized (container) {
                    for (int i = 0; i < container.getOutputs().size(); i++) {
                        var output = container.getOutputs().get(i);
                        outputPrinter.printStandard(output, outputThreadBuffer);
                        if (printError)
                            outputPrinter.printError(output, outputThreadBuffer);
                    }
                }
            }
        });
        t.setDaemon(true);
        this.t=t;
    }
}
