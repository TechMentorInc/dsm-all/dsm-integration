package us.techmentor.dsmintegration;

import us.techmentor.dsmintegration.output.Outputs;

import java.util.List;

import static us.techmentor.dsmintegration.LibraryMaintenance.*;

public class DSMContainer {
    String name;
    String host="";
    String shared="";
    String filter="";

    public Docker docker;


    public List<Outputs> getOutputs(){
        return docker.registeredOutputs;
    }
    public DSMContainer(String name, String shared,String filter){
        this.name=name;
        this.shared=shared;
        this.docker = new Docker(name,shared);
        this.filter=filter;
    }

    public void setHost(String host){
        this.host = host;
    }


    protected String filterSwitch(){
        if(filter!=null && !filter.isEmpty()) return "--log-filter="+filter;
        return "";
    }
    protected String getHostCommand(){
        if(!host.equals("")){
            return "-H ssh://ec2-user@"+host+" ";
        }
        return "";
    }

    public String start(){
        if(shared.equals("")) {
            prepareDebianInstallation();
            var imageName = docker.buildImage(this.getHostCommand());
            docker.startMachine(this.getHostCommand(), imageName);
        }else{
            docker.createDirectoryOnSharedMachine(this.getHostCommand(),this.name);
        }
        return getIpAddress();
    }

    public String getIpAddress() {
        var n = shared.equals("")?name:shared;
        var cmd = "docker "+getHostCommand()+"  inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "+n;

        String output = "";
        int i = 0;
        while(i<10 && (output==null || output.trim().equals(""))){
            output = docker.runDockerCommandReadFirstString(cmd);
            betterSleep(200);
            i++;
        }
        return output;
    }

    public void getActorInformation(String actorName){
        docker.runShellCommand(getHostCommand(),"dsm actor show "+actorName+"");
    }
    public void getDirectoryContents(){
        docker.runShellCommand(getHostCommand(),"ls -a");
    }
    public void end(){
        var cmd = "docker "+getHostCommand()+" kill "+name+"; docker "+getHostCommand()+" rm "+name;
        docker.runDockerCommand(cmd);
    }
    public void addRemote(String ip){
        docker.runShellCommand(getHostCommand(),"dsm remote add -a "+ip);
    }
    public void serve(){
        serve(0,5000);
    }

    public void serve(int timeoutSeconds) { serve(timeoutSeconds,5000); }
    public void serve(int timeoutSeconds, int synchPeriod){
        String timeoutParameter = "";
        String synchPeriodParameter = " --synch-period="+synchPeriod;
        if(timeoutSeconds>0)
             timeoutParameter = " --timeout="+timeoutSeconds;
        docker.runShellCommand(getHostCommand(),"dsm "+filterSwitch()+" "+timeoutParameter+synchPeriodParameter+" serve");
    }
    public void postStatus(String user, String password, String status){
        docker.runShellCommand(
                getHostCommand(),
                "dsm "+filterSwitch()+" timeline add -u=\""+user+"\" -p=\""+password+"\" -m=\""+status+"\""
        );
    }
    public void follow(String user, String password, String whoToFollow){
        docker.runShellCommand(
                getHostCommand(),
                "dsm "+filterSwitch()+" actor follow -u=\""+user+"\" -p=\""+password+"\" "+whoToFollow);
    }
    public void readTimeline(String user, String password) {
        docker.runShellCommand(
                getHostCommand(),
                "dsm "+filterSwitch()+" timeline read -u=\""+user+"\" -p=\""+password+"\"");
    }
    public void createActor(String user, String password) {
        docker.runShellCommand(
                getHostCommand(),
                "dsm "+filterSwitch()+" actor add -u='"+user+"' -p='"+password+"'");
    }

    public void listActors(String user, String password) {
        docker.runShellCommand(
                getHostCommand(),
                "dsm "+filterSwitch()+" actor list -u='"+user+"' -p='"+password+"'");
    }
    public void betterSleep(int millis){
        try{
            Thread.sleep(millis);
        }catch(Throwable t){ throw new RuntimeException(t); }
    }
}
