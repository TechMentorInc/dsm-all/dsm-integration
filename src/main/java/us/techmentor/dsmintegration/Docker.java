package us.techmentor.dsmintegration;

import us.techmentor.dsmintegration.output.Outputs;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Docker {
    String name=null;
    String shared=null;

    public List<Outputs> registeredOutputs= new ArrayList<>();

    public Docker(String name, String displayName){
        this.name = name;
        this.shared=displayName;
    }

    public Outputs registerOutputs(Process process,String commandLine){
        var standard = process.getInputStream();
        var error = process.getErrorStream();
        var outputs = new Outputs(name,shared,commandLine,standard,error);

        registeredOutputs.add(outputs);
        return outputs;
    }
    public byte[] read(InputStream inputStream) throws IOException {
        return read(inputStream,0);
    }
    public byte[] read(InputStream inputStream,int howmany) throws IOException {
        while(!(inputStream.available()>howmany));
        var bytes = new byte[inputStream.available()];
        inputStream.mark(inputStream.available()+100);
        inputStream.read(bytes);
        inputStream.reset();
        return bytes;
    }
    public String runDockerCommandReadFirstString(String cmd) {
        return runDockerCommandReadFirstString(cmd,0);
    }
    public String runDockerCommandReadFirstString(String cmd, int howmany){
        try{
            var p = Runtime.getRuntime().exec(
                    new String[]{
                            "/bin/bash",
                            "-c",
                            cmd
                    }
            );

            var input = new String(read(p.getInputStream(),howmany));
            registerOutputs(p,cmd);
            return input.substring(0,input.trim().length());
        }catch(Throwable t){
            throw new RuntimeException(t);
        }
    }
    public void runDockerCommand(String cmd){
        try{
            var p = Runtime.getRuntime().exec(
                    new String[]{
                            "/bin/bash",
                            "-c",
                            cmd
                    }
            );
            registerOutputs(p,cmd);
        }catch(Throwable t){
            throw new RuntimeException(t);
        }
    }

    public String buildImage(String hostCommand) {
        return runDockerCommandReadFirstString("docker "+hostCommand+" build -q .");
    }

    public void startMachine(String hostCommand, String imageName) {
        var n = shared.equals("")?name:shared;
        this.runDockerCommand("docker "+hostCommand+" run --net bridge -dit --name "+n+" "+imageName+" /bin/bash -c 'mkdir "+name+"; cd "+name+"; dsm init;/bin/bash; cd ..'");
    }

    public void createDirectoryOnSharedMachine(String hostCommand,String dir){
        var n = shared.equals("")?name:shared;
        this.runDockerCommand("docker "+hostCommand+" exec -i "+n+" /bin/bash -c 'mkdir "+dir+"' ");
    }
    public void runShellCommand(String hostCommand, String shellCommand){
        var n = shared.equals("")?name:shared;
        var cmd = "docker "+hostCommand+" exec -i "+n+" /bin/bash -c 'cd "+name+"; "+shellCommand+"; cd ..'";
        this.runDockerCommand(cmd);
    }
}
