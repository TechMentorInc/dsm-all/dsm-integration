package us.techmentor.dsmintegration.creation;

import us.techmentor.dsmintegration.AWSDSMContainer;
import us.techmentor.dsmintegration.DSMContainer;
import us.techmentor.dsmintegration.cloud.AWS;
import us.techmentor.dsmintegration.output.OutputThread;
import us.techmentor.dsmintegration.output.OutputThreadBuffer;

import java.lang.ref.Cleaner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class ContainerCreator {
    public static final List<Runnable> shutdowns = new CopyOnWriteArrayList<Runnable>();

    DSMContainer container = null;
    OutputThread outputThread = new OutputThread();
    boolean printErrorOn=false;
    boolean showOutputs=false;
    String name;
    AWS aws;
    String shared="";
    String filter="";

    public  DSMContainer create(){
        DSMContainer dsmContainer;
        if(aws!=null) {
            dsmContainer = new AWSDSMContainer(name,shared);
            var instance = aws.getInstance();
            dsmContainer.setHost(instance.ip());
        }else{
            dsmContainer = new DSMContainer(name,shared,filter);
        }

        container = dsmContainer;
        if(showOutputs){
            outputThread.create(printErrorOn,container);
            outputThread.start();
            buffers.put(this.name,outputThread.getBuffer());
        }

        final Runnable s = (() -> {
            System.out.println("Cleaning Container Creation..");
            outputThread.stop();
            container.end();
            buffers.remove(this.name);
        });

        shutdowns.add(s);
        return dsmContainer;
    }
    public ContainerCreator printError(boolean printErrorOn)
    {
        this.printErrorOn = printErrorOn;
        return this;
    }
    public ContainerCreator showOutput() {
        showOutputs=true;
        return this;
    }
    public ContainerCreator withName(String name){
        this.name = name;
        return this;
    }
    public ContainerCreator withFilter(String filter){
        this.filter=filter;
        return this;
    }
    public ContainerCreator shareBufferWith(String name){
        var buffer = buffers.get(name);
        this.outputThread = buffer.getOutputThread();
        buffers.put(this.name,buffer);
        return this;
    }
    public ContainerCreator onAWS(){
        this.aws = AWS.getAWS();
        return this;
    }

    public static Map<String, OutputThreadBuffer> buffers = new HashMap<>();
    public static OutputThreadBuffer findBufferForContainer(String name) {
        return buffers.get(name);
    }

    public ContainerCreator shareMachineWith(String name) {
        this.shared=name;
        return this;
    }
}
