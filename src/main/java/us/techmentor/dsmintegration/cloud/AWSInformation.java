package us.techmentor.dsmintegration.cloud;

public class AWSInformation {
    public static boolean awsFlagsAvailable(){
        return (System.getProperty("AWS_ACCESS_KEY")!=null &&
            System.getProperty("AWS_SECRET_KEY")!=null &&
            System.getProperty("AWS_INSTANCE_TYPE")!=null &&
            System.getProperty("AWS_SECURITY_GROUP_ID")!=null &&
            System.getProperty("AWS_KEYPAIR_NAME")!=null &&
            System.getProperty("AWS_PRIVATEKEY_FILE")!=null);
    }
}
