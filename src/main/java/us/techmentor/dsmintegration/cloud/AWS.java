package us.techmentor.dsmintegration.cloud;
import org.apache.http.util.ByteArrayBuffer;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.*;
import com.jcraft.jsch.*;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import static us.techmentor.dsmintegration.LibraryMaintenance.prepareDebianInstallation;

public class AWS {
    static final String reallySmallLinuxImageAMIid = "ami-0aeeebd8d2ab47354";
    String accessKey;
    String secretKey;
    String instanceType;
    String securityGroupID;
    String keyPairName;
    String privateKeyFile;
    Instance instance;

    private AWS(){
        accessKey = System.getProperty("AWS_ACCESS_KEY");
        secretKey = System.getProperty("AWS_SECRET_KEY");
        instanceType = System.getProperty("AWS_INSTANCE_TYPE");
        securityGroupID = System.getProperty("AWS_SECURITY_GROUP_ID");
        keyPairName = System.getProperty("AWS_KEYPAIR_NAME");
        privateKeyFile = System.getProperty("AWS_PRIVATEKEY_FILE");
    }

    private static AWS aws=null;
    public static AWS getAWS(){
        if(aws==null){
            aws = new AWS();
        }
        return aws;
    }

    public record Instance (String id, String ip){}

    public Ec2Client createClient(){
        return Ec2Client.builder()
                .region(Region.US_EAST_1)
                .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKey, secretKey)))
                .build();
    }
    public Instance findInstance(){
        var ec2Client = createClient();
        String tagKey = "dsmTest";
        String tagValue = "Scale";
        Filter tagFilter = Filter.builder()
                .name("tag:" + tagKey)
                .values(tagValue)
                .build();

        Filter runningFilter = Filter.builder()
                .name("instance-state-name")
                .values("running")
                .build();

        DescribeInstancesRequest describeInstancesRequest = DescribeInstancesRequest.builder()
                .filters(tagFilter,runningFilter)
                .build();

        DescribeInstancesResponse response = ec2Client.describeInstances(describeInstancesRequest);

        List<Reservation> reservations = response.reservations();
        for (Reservation reservation : reservations) {
            List<software.amazon.awssdk.services.ec2.model.Instance> instances = reservation.instances();
            for (software.amazon.awssdk.services.ec2.model.Instance i : instances) {
                ec2Client.close();
                return new Instance(i.instanceId(),i.publicIpAddress());
            }
        }
        ec2Client.close();
        return null;
    }
    public Instance getInstance(){
        if(instance==null) {
            instance = aws.findInstance();
            if(instance == null) {
                instance = aws.createAndStartInstance();

                boolean ready = false;
                for (int i = 0; i < 100 && !ready; i++) {
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    if (aws.instanceExists(instance.id())) ready = true;
                }
                aws.prepareDocker(instance);
            }
        }
        return instance;
    }
    public Instance createAndStartInstance() {
        var ec2Client = createClient();
        Tag tag = Tag.builder()
                .key("dsmTest")
                .value("Scale")
                .build();

        RunInstancesRequest runRequest = RunInstancesRequest.builder()
                .imageId(reallySmallLinuxImageAMIid)
                .instanceType(instanceType)
                .tagSpecifications(
                TagSpecification.builder()
                        .resourceType(ResourceType.INSTANCE)
                        .tags(tag)
                        .build())
                .maxCount(1)
                .minCount(1)
                .keyName(keyPairName)
                .securityGroupIds(securityGroupID)
                .build();

        RunInstancesResponse response = ec2Client.runInstances(runRequest);
        var instance = response.instances().get(0);

        String instanceId = instance.instanceId();
        String instanceIp = instance.publicIpAddress();
        for(int i=0; i<40 && (instanceIp==null || instanceIp.equals("")); i++){
            try {
                Thread.sleep(2000);
                instanceIp = getInstanceIP(instanceId);
            }catch(Throwable t) { throw new RuntimeException(t); }
        }

        if(instanceIp==null || instanceIp.equals(""))
            throw new RuntimeException("Unable to get Public IP");

        System.out.println("Created instance with ID: " + instanceId);


        StartInstancesRequest startRequest = StartInstancesRequest.builder()
                .instanceIds(instanceId)
                .build();
        ec2Client.startInstances(startRequest);

        System.out.println("Instance started.");
        return new Instance(instanceId,instanceIp);
    }

    public void terminateInstance(Instance instance){
        var ec2Client = createClient();
        TerminateInstancesRequest terminateInstancesRequest =
                TerminateInstancesRequest.builder()
                .instanceIds(instance.id)
                .build();

         ec2Client.terminateInstances(terminateInstancesRequest);
    }
    public boolean instanceExists(String instanceId) {
        var ec2Client = createClient();
        var describeInstanceRequest = DescribeInstanceStatusRequest.builder().instanceIds(instanceId).build();
        var describeStatusResponse = ec2Client.describeInstanceStatus(describeInstanceRequest);
        for (var status : describeStatusResponse.instanceStatuses()){
            if(status.instanceId().equals(instanceId))
                if(status.instanceStatus().status().toString().equals("ok")){
                    return true;
                }
        }
        return false;
    }
    public String getInstanceIP(String instanceId){
        var ec2Client = createClient();
        var describeInstances = ec2Client.describeInstances();

        StringBuffer result = new StringBuffer();
        describeInstances.reservations().forEach(
        r -> r.instances().forEach(
            i-> {
                if(i.instanceId().equals(instanceId)){
                    result.delete(0,result.length());
                    result.append(i.publicIpAddress());
                }
            })
        );
        return result.toString();
    }
    public void prepareDocker(Instance instance){
        JSch jSch = new JSch();
        prepareDebianInstallation();
        try {
            jSch.addIdentity(privateKeyFile);

            Session session = jSch.getSession("ec2-user", instance.ip(), 22);
            session.setConfig("StrictHostKeyChecking", "no"); // Disable strict host key checking
            session.connect();

            Runtime.getRuntime().exec(
                new String[]{
                    "/bin/bash",
                    "-c",
                    "docker build -q . -t test-image; docker save test-image -o test-image.tar"
                }
            ).waitFor();

            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");

            channelSftp.setOutputStream(new OutputStream() {
                @Override
                public void write(int b) throws IOException {
                    System.out.write(b);
                }
            });
            channelSftp.connect();
            channelSftp.put("test-image.tar", "test-image.tar");
            channelSftp.exit();
            channelSftp.disconnect();

            String installDockerCommands = "sudo yum update -y\n" +
                    "sudo amazon-linux-extras install docker -y\n" +
                    "sudo service docker start\n" +
                    "sudo docker load -i test-image.tar\n"+
                    "sudo usermod -a -G docker ec2-user\n"; // Adjust the user as needed

            ChannelExec channel = (ChannelExec) session.openChannel("exec");
            channel.setOutputStream(new OutputStream() {
                @Override
                public void write(int b) throws IOException {
                    System.out.write(b);
                }
            });
            channel.setCommand(installDockerCommands);
            channel.connect();

            while (!channel.isClosed()) {Thread.sleep(100);}

            channel.disconnect();
            session.disconnect();

            System.out.println("Docker has been installed on the EC2 instance.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

