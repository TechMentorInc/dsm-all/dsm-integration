package us.techmentor.dsmintegration;

import org.junit.*;
import us.techmentor.dsmintegration.cloud.AWS;
import us.techmentor.dsmintegration.creation.ContainerCreator;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.awaitility.Awaitility.with;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static us.techmentor.dsmintegration.cloud.AWSInformation.awsFlagsAvailable;

public class AWSTest {
    AWS.Instance instance=null;
    private static final int TIMEOUT=1200;

    @BeforeClass
    public static void startup(){
        // Really, surely, verily, truly, get rid of them resources
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> ContainerCreator.shutdowns.forEach(s->s.run())));
    }
    
    @Before
    public void init(){
        Assume.assumeTrue(awsFlagsAvailable());
        var aws = AWS.getAWS();
        instance = aws.getInstance();
    }

    @After
    public void closeOut(){
        var leaveMachineRunning = System.getProperty("DSM_LEAVE_INSTANCE_RUNNING");
        leaveMachineRunning = leaveMachineRunning==null?"false":leaveMachineRunning;
        if(!leaveMachineRunning.equals("true"))
            AWS.getAWS().terminateInstance(instance);
    }
//    @Test
    public void bunchOfArbitraryActionFromManyNodes(){
        // we'll need a mechanism to
        // - provide a script
        // - run in a configurable number of threads
        // - randomize / vary the time between interactions
        // - not be particularly verbose
        // - this might be the time to flip over to scala

        // first arrangement of nodes should be:
        // - hub and spokes

        // keep an eye on the perf for the hub
        // - do I know how to connect a profiler/debugger?
        // - am I capturing enough in-app telemetry
        //   * connections, speed, memory, what else?
        //   * do I need some plumbing for this?
        // - don't forget - one piece of telemetry, the fact-to-chain time
        //   will be necessary for the complexity negotiation.
    }
    @Test
    public void dsmInitTest() throws InterruptedException
    {
        System.out.println("Creating");

        var container1 =
                new ContainerCreator()
                        .printError(true)
                        .showOutput()
                        .withName("test_1")
                        .onAWS()
                        .create();

        System.out.println("Starting");
        container1.start();
        Thread.sleep(4000);

        container1.getDirectoryContents();
        Thread.sleep(4000);
        container1.end();

        var result = ContainerCreator.findBufferForContainer("test_1");
        assertTrue(result.asString().contains(".dsm"));
    }
    @Test
    public void testLinearNetwork_RecommendC38xLarge() throws Exception{
        int SIZE = 50;
        var containers = createContainers(SIZE,true).toArray(new DSMContainer[0]);
        for(int i=0; i< containers.length;i++){
            containers[i].serve(100);
            if(i>0)
                containers[i-1].addRemote(containers[i].getIpAddress());
        }
        try {
            final var finalBuffer = ContainerCreator.findBufferForContainer("dsm_machine_1");

            containers[0].createActor("Kyle","secret1");
            await().atMost(TIMEOUT, SECONDS).until(()->finalBuffer.asString().contains("User created: Kyle"));

            containers[SIZE-1].createActor("Shaina","secret2");
            await().atMost(TIMEOUT, SECONDS).until(()->finalBuffer.asString().contains("User created: Shaina"));

            containers[SIZE-1].follow("Shaina","secret2","Kyle");
            with().pollInterval(5,SECONDS).and().with().pollDelay(2,SECONDS).await().atMost(TIMEOUT, SECONDS).until(
            ()->{
                var result = finalBuffer.asString().contains("Successfully followed - Kyle");
                if (!result) containers[SIZE-1].follow("Shaina","secret2","Kyle");
                return result;
            });

            containers[0].postStatus("Kyle","secret1","just setting up my dsm");
            with().pollInterval(2,SECONDS).and().with().pollDelay(2,SECONDS).await().atMost(TIMEOUT, SECONDS).until(
                ()->finalBuffer.asString().contains("Successfully posted message."));

            with().pollInterval(5,SECONDS).and().with().pollDelay(10,SECONDS).await().atMost(TIMEOUT, SECONDS).until(
                    ()->{
                        containers[SIZE-1].readTimeline("Shaina","secret2");
                        var str = finalBuffer.asString();
                        return str.contains("Post by Kyle:") && str.contains("\t>> just setting up my dsm");
                    }
            );
        } finally {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for(int i=0; i<SIZE; i++) {
                containers[i].end();
            }
            stopContainers(containers);
        }
    }

    public void stopContainers(DSMContainer[] containers){
        for(int i=0; i< containers.length; i++){
            containers[i].end();
        }
    }

    public List<DSMContainer> createContainers(int howMany, boolean start){
        List<DSMContainer> retval = new ArrayList<>();
        retval.add(
            new ContainerCreator()
                .withName("dsm_machine_1")
//                .printError()
                .showOutput()
                .onAWS()
                .create()
        );
        for(int i=1; i<howMany; i++){
//            if(i==1 || i==9)
            retval.add(new ContainerCreator()
                .withName("dsm_machine_"+(i+1))
                .shareBufferWith("dsm_machine_1")
//                .printError()
                .showOutput()
                .onAWS()
                .create());
//            else
//            retval.add(new ContainerCreator()
//                .withName("dsm_machine_"+(i+1))
//                .shareBufferWith("dsm_machine_1")
//                .showOutput()
//                .onAWS()
//                .create());
        }
        if(start)
            retval.stream().forEach(x->x.start());
        return retval;
    }
}
