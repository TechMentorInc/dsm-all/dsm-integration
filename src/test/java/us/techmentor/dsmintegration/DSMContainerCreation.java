package us.techmentor.dsmintegration;

import us.techmentor.dsmintegration.creation.ContainerCreator;

import java.util.ArrayList;
import java.util.List;

public class DSMContainerCreation {
    public static List<DSMContainer> createBasicContainersWithSharedBuffer(int howMany){
        return createBasicContainersWithSharedBuffer(howMany,true,"");
    }
    public static List<DSMContainer> createBasicContainersWithSharedBuffer(int howMany,String logs){
        return createBasicContainersWithSharedBuffer(howMany,true,logs);
    }
    public static List<DSMContainer> createBasicContainersWithSharedBuffer(int howMany, boolean showError,String logs){
        List<DSMContainer> retval = new ArrayList<>();
        retval.add(
                new ContainerCreator()
                        .withName("dsm_machine_1")
                        .printError(showError)
                        .showOutput()
                        .withFilter(logs)
                        .create()
        );
        for(int i=1; i<howMany; i++){
            retval.add(new ContainerCreator()
                    .withName("dsm_machine_"+(i+1))
                    .shareBufferWith("dsm_machine_1")
                    .printError(showError)
                    .showOutput()
                    .withFilter(logs)
                    .create());
        }
        return retval;

    }
    public static List<DSMContainer> createBasicContainersWithSharedMachineAndBuffer(int howMany, int startIndex){
        return createBasicContainersWithSharedMachineAndBuffer(howMany,startIndex,true,"");
    }

    public static List<DSMContainer> createBasicContainersWithSharedMachineAndBuffer(int howMany, int startIndex,String logs){
        return createBasicContainersWithSharedMachineAndBuffer(howMany,startIndex,true,logs);
    }

    public static List<DSMContainer> createBasicContainersWithSharedMachineAndBuffer(int howMany, int startIndex, boolean showError,String logs){
        List<DSMContainer> retval = new ArrayList<>();
        retval.add(
                new ContainerCreator()
                        .withName("dsm_machine_"+startIndex)
                        .printError(showError)
                        .showOutput()
                        .withFilter(logs)
                        .create()
        );
        for(int i=startIndex; i<howMany+startIndex-1; i++){
            retval.add(new ContainerCreator()
                    .withName("dsm_machine_"+(i+1))
                    .shareBufferWith("dsm_machine_"+startIndex)
                    .shareMachineWith("dsm_machine_"+startIndex)
                    .printError(showError)
                    .withFilter(logs)
                    .showOutput()
                    .create());
        }
        return retval;
    }

    public static void startContainerWithRemote(DSMContainer container, String remote){
        container.start();
        container.addRemote(remote);
    }

    public static void startContainersWithRemote(List<DSMContainer> containers, String remote){
        for(var c : containers){
            startContainerWithRemote(c, remote);
        }
    }

    public static String startAndServeReturnIP(DSMContainer container){
        var ip = container.start();
        container.serve(60);
        return ip;
    }

    public static void containersToServe(List<DSMContainer> containers){
        for(var c : containers) c.serve(50);
    }
}
