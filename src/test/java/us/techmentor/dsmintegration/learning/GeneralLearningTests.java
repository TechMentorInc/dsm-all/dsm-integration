package us.techmentor.dsmintegration.learning;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.apache.http.util.ByteArrayBuffer;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import us.techmentor.dsmintegration.AWSDSMContainer;
import us.techmentor.dsmintegration.creation.ContainerCreator;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class GeneralLearningTests {
//    @Test
    public void sftpTest(){
        JSch jSch = new JSch();
        try {
            jSch.addIdentity("/Users/kyle/code/techmentor-ec2-general.pem");
            Session session = jSch.getSession("ec2-user", "52.207.61.219", 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();

            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();
            channelSftp.put("test-image.tar", "test-image.tar");
            channelSftp.exit();
            channelSftp.disconnect();
            session.disconnect();
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }
//    @Test
    public void runCommandTest(){
        try {
            var p = Runtime.getRuntime().exec(
                new String[]{
                    "/bin/bash",
                    "-c",
                    "docker build -q . -t test-image; docker save test-image -o test-image.tar"
                }
            );

            p.waitFor();

            var inp = p.getInputStream();
            var err = p.getErrorStream();

            System.out.println("Beginning Transfer.");
            var b = new ByteArrayBuffer(10);
            boolean done =false;
            while(!done){
                var bytes = new byte[inp.available()];
                inp.read(bytes);
                b.append(bytes,0,bytes.length);

                var bytes2 = new byte[err.available()];
                err.read(bytes2);
                b.append(bytes2,0,bytes2.length);

                if(bytes2.length==0 && bytes.length==0) done=true;
            }
            System.out.println(new String(b.toByteArray()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void executingRemoteDockerCommand() throws Exception{
        var c = new AWSDSMContainer("remote-test","");
        c.setHost("54.208.124.67");

        c.start();

    }
    @Test
    public void executingRemoteDockerCommand2() throws Exception{
        var c = new AWSDSMContainer("remote-test","");

        c.setHost("54.208.124.67");

        c.getDirectoryContents();

        Thread.sleep(5000);

        byte[] standard = new byte[4096];
        byte[] error = new byte[4096];
        c.getOutputs().get(0).standard().read(standard);
        c.getOutputs().get(0).error().read(error);

        System.out.println("<<<Standard>>>");
        System.out.println(new String(standard));

        System.out.println("<<<Error>>>");
        System.out.println(new String(error));
    }

    @Test
    public void addCategoryToLogMessage(){
        var captureSystemOut = new CaptureSystemOut();
        captureSystemOut.capture();
        configureLogging();

        var logger = LoggerFactory.getLogger(this.getClass());

        MDC.put("category","tcp");
        logger.error("categorized error");

        MDC.clear();
        logger.error("random error");

        assertTrue(
            captureSystemOut
                .capturedOutput
                .toString()
                .contains("categorized"));

        assertFalse(
            captureSystemOut
                .capturedOutput
                .toString()
                .contains("random"));


    }
    public class CaptureSystemOut{
        public final StringBuilder capturedOutput = new StringBuilder();
        private final PrintStream originalOut = System.out;

        public void capture() {
            System.setOut(new PrintStream(new OutputStream() {
                @Override
                public void write(int b) {
                    capturedOutput.append((char) b);
                    originalOut.write(b);
                }
            }));
        }
    }
    public void configureLogging(){
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);

        MDCFilter mdcFilter = new MDCFilter("tcp");
        mdcFilter.start();

        for (Iterator<Appender<ILoggingEvent>> it = rootLogger.iteratorForAppenders(); it.hasNext(); ) {
            Appender<ILoggingEvent> appender = it.next();
            appender.addFilter(mdcFilter);
        }
    }

    public class MDCFilter extends Filter<ILoggingEvent> {
        private final String requiredCategory;

        public MDCFilter(String requiredCategory) {
            this.requiredCategory = requiredCategory;
        }

        @Override
        public FilterReply decide(ILoggingEvent event) {
            String mdcCategory = MDC.get("category");
            if (requiredCategory.equals(mdcCategory)) {
                return FilterReply.ACCEPT;
            }
            return FilterReply.DENY;
        }
    }
}
