package us.techmentor.dsmintegration;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import us.techmentor.dsmintegration.creation.ContainerCreator;
import us.techmentor.dsmintegration.output.OutputThreadBuffer;
import java.util.List;
import java.util.regex.Pattern;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.awaitility.Awaitility.with;
import static us.techmentor.dsmintegration.DSMContainerCreation.*;

public class BasicDSMTest
{
    private static final int TIMEOUT=240;
    private static void doShutdowns(){
        ContainerCreator.shutdowns.forEach(
        s->{
            s.run();
            ContainerCreator.shutdowns.remove(s);
        });
        try{ Thread.sleep(10000); }
        catch(Throwable t) {}
    }

    @BeforeClass
    public static void classStartup(){
        Runtime.getRuntime().addShutdownHook(
        new Thread(() -> doShutdowns()));
    }

    @Before
    public void startup(){
        doShutdowns();
    }

    @Test
    public void dsmInitTest() throws InterruptedException
    {
        System.out.println("Creating");

        var container1 =
            new ContainerCreator()
                .printError(true)
                .showOutput()
                .withName("test_1")
                .create();

        System.out.println("Starting");
        container1.start();

        try {
            final var result = ContainerCreator.findBufferForContainer("test_1");
            with().pollInterval(5, SECONDS).and().with().pollDelay(5, SECONDS).await().atMost(TIMEOUT, SECONDS).until(
                () -> {
                    container1.getDirectoryContents();
                    return result.asString().contains(".dsm");
                }
            );
        } finally {
            container1.end();
        }
    }

    private Pattern p(String toCompile){ return Pattern.compile(toCompile); }
    private void waitAndCheck(OutputThreadBuffer buffer, Pattern pattern){
        await().atMost(TIMEOUT, SECONDS).until(
                ()-> pattern.matcher(buffer.asString()).find());
    }
    @Test
    public void dsmStartingWithLongChain() throws InterruptedException {
        var serverContainer = createBasicContainersWithSharedBuffer(1,true, "").get(0);
        var containers = createBasicContainersWithSharedMachineAndBuffer(1,2,true,"");

        System.out.println("Starting server machine");
        var serverIp = startAndServeReturnIP(serverContainer);

        waitAndCheck(
                ContainerCreator.findBufferForContainer("dsm_machine_1"),
                Pattern.compile("Waiting for connections..."));

        serverContainer.createActor("Kyle","secret1");
        System.out.println("Created Actor on server");
        Thread.sleep(3000);

        System.out.println("Posting Status");
        serverContainer.postStatus("Kyle","secret1","hello world");
        Thread.sleep(3000);

        System.out.println("Starting dsm 2 & 3 - same machine");
        startContainersWithRemote(containers,serverIp);

        waitAndCheck(
                ContainerCreator.findBufferForContainer("dsm_machine_2"),
                p("dsm_machine_2/.*Added remote \\["+serverIp+"\\]"));


        containers.get(0).createActor("Shaina","secret1");
        Thread.sleep(5000);

        System.out.println("Reading Timeline");
        containers.get(0).readTimeline("Shaina","secret1");
        Thread.sleep(3000);

        containers.get(0).follow("Shaina","secret1","Kyle");
        Thread.sleep(3000);

        System.out.println("Checking client...");
        waitAndCheck(
                ContainerCreator.findBufferForContainer("dsm_machine_2"),
                p("hello world"));
    }
    @Test
    public void dsmMultipleConnectionsFromSameIP() throws InterruptedException {
        var serverContainer = createBasicContainersWithSharedBuffer(1,true, "").get(0);
        var containers = createBasicContainersWithSharedMachineAndBuffer(2,2,true,"");

        System.out.println("Starting server machine");
        var serverIp = startAndServeReturnIP(serverContainer);

        waitAndCheck(
            ContainerCreator.findBufferForContainer("dsm_machine_1"),
            Pattern.compile("Waiting for connections..."));

        System.out.println("Starting dsm 2 & 3 - same machine");
        startContainersWithRemote(containers,serverIp);

        waitAndCheck(
            ContainerCreator.findBufferForContainer("dsm_machine_2"),
            p("dsm_machine_2/.*Added remote \\["+serverIp+"\\]"));

        serverContainer.createActor("Kyle","secret1");
        System.out.println("Created Actor on server");
        Thread.sleep(8000);

        containers.get(0).createActor("Shaina","secret1");
        Thread.sleep(8000);

        System.out.println("Reading Timeline");
        containers.get(0).readTimeline("Shaina","secret1");
        Thread.sleep(8000);

        containers.get(0).follow("Shaina","secret1","Kyle");
        Thread.sleep(3000);

        System.out.println("Posting Status");
        serverContainer.postStatus("Kyle","secret1","hello world");
        Thread.sleep(3000);

        System.out.println("Checking client...");
        waitAndCheck(
            ContainerCreator.findBufferForContainer("dsm_machine_2"),
            p("hello world"));
    }

    @Test
    public void dsmConnectionTimeout() throws InterruptedException {
        List<DSMContainer> containers = null;
        OutputThreadBuffer buffer     = null;
        try {
            containers = createBasicContainersWithSharedBuffer(2,true,"");
            buffer = ContainerCreator.findBufferForContainer("dsm_machine_1");
            final var finalBuffer = buffer;

            System.out.println("Starting machine 1");
            var machine1Ip = containers.get(0).start();

            System.out.println("Starting serve machine 1");
            containers.get(0).serve(20);

            await().atMost(TIMEOUT, SECONDS).until(()-> finalBuffer.asString().contains("Waiting for connections..."));

            System.out.println("Starting machine 2");
            var machine2Ip = containers.get(1).start();

            containers.get(1).addRemote(machine1Ip);
            containers.get(1).serve();
            System.out.println("Awaiting registration..");
            await().atMost(TIMEOUT, SECONDS).until(()->finalBuffer.asString().contains("Registered (peer="));

            System.out.println("Ending 1");
            containers.get(1).end();

            System.out.println("Awaiting Unregister..");
            await().atMost(TIMEOUT, SECONDS).until(()->finalBuffer.asString().contains("UNREGISTERED: [" + machine2Ip + "]"));

            System.out.println("Ending 0");
            containers.get(0).end();
        } catch(Throwable t){
            t.printStackTrace();
        } finally {
            containers.get(0).end();
            containers.get(1).end();
            buffer.stop();
        }
    }

    @Test
    public void dsmAddActorOnClient() throws InterruptedException{
        List<DSMContainer> containers = null;
        OutputThreadBuffer buffer     = null;
        try {
            containers = createBasicContainersWithSharedBuffer(2,"network");
            buffer =
            ContainerCreator
                .findBufferForContainer("dsm_machine_1");

            final var finalBuffer = buffer;

            System.out.println("Starting machine 1");
            var machine1Ip = containers.get(0).start();

            System.out.println("Starting serve machine 1");
            containers.get(0).serve(20);
            await().atMost(TIMEOUT, SECONDS).until(()->finalBuffer.asString().contains("Waiting for connections..."));

            System.out.println("Starting machine 2");
            containers.get(1).start();
            containers.get(1).addRemote(machine1Ip);
            containers.get(1).serve(20);
            await().atMost(TIMEOUT, SECONDS).until(()->finalBuffer.asString().contains("Added remote ["));

            containers.get(1).createActor("Kyle","secret1");

            System.out.println("Created Actor on client");

            await().atMost(TIMEOUT, SECONDS).until(()->finalBuffer.asString().contains("User created: Kyle"));
            System.out.println("Checking server...");

            var host2 = containers.get(0);
            with().pollInterval(5,SECONDS).and().with().pollDelay(5,SECONDS).await().atMost(TIMEOUT, SECONDS).until(
                ()->{
                    host2.getActorInformation("Kyle");
                    return finalBuffer.asString().contains("Name:\t\t\tKyle");
                }
            );

        } finally {
            try {
                Thread.sleep(2000);
                containers.get(0).end();
                containers.get(1).end();
                buffer.stop();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void createActorCheckUserList() throws InterruptedException{
        List<DSMContainer> containers = null;
        OutputThreadBuffer buffer = null;
        try {
            containers = createBasicContainersWithSharedBuffer(1);
            buffer =
            ContainerCreator
                .findBufferForContainer("dsm_machine_1");
            final var finalBuffer = buffer;

            System.out.println("Starting machine 1");
            containers.get(0).start();

            containers.get(0).createActor("Kyle","secret1");
            await().atMost(TIMEOUT, SECONDS).until(()->finalBuffer.asString().contains("User created: Kyle"));

            containers.get(0).listActors("Kyle","secret1");
            await().atMost(TIMEOUT, SECONDS).until(()->finalBuffer.asString().contains("User: Kyle"));

            containers.get(0).end();
            buffer.stop();
        } finally {
            try {
                Thread.sleep(2000);
                containers.get(0).end();
                buffer.stop();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
