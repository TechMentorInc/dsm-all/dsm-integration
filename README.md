# DSM Integration
## Getting started
To get started you need to pull in the "debian" installation package - and put it in the root of the working directory of this project.
<br>
<br>
You can copy this from the dsm project (it's part of the output of the build).
<br><br>
Or you can pull from our repository:
<br>`mvn install -U -Dmaven.test.skip=true`
<br><br>
For the scale tests to run, the following AWS information must be configured when invoking JVM:<br>
`-DAWS_ACCESS_KEY=<ACCESSKEY> -DAWS_SECRET_KEY=<SECRETKEY> -DAWS_KEYPAIR_NAME=<KEYPAIR_NAM> -DAWS_INSTANCE_TYPE=t2.nano -DAWS_SECURITY_GROUP_ID=<GROUP ID> -DAWS_PRIVATEKEY_FILE=~/privatekey.pem`
<br>
<br>
If any of these aren't configured the tests will abort without failing - so it shouldn't keep the suite from running, but it won't try to do any cloud magic.
<br><br>
In order to prevent termination of the test machine at the end of the suite, set the following:
`-DDSM_LEAVE_INSTANCE_RUNNING=true`
<br><br>
Also: strict host checking should be turned off for the user context the tests will be running under.
<br>
<br>
Also +1: `ssh-add ~/privatekey.pem` on your local machine will be necessary for the remote docker command to run.